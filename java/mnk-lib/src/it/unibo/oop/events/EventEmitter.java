package it.unibo.oop.events;

public interface EventEmitter<Arg> {
    void emit(Arg data);

    EventSource<Arg> getEventSource();

    static <X> EventEmitter<X> ordered() {
        return new OrderedEventSourceImpl<>();
    }

    static <X> EventEmitter<X> unordered() {
        return new UnorderedEventSourceImpl<>();
    }
}
