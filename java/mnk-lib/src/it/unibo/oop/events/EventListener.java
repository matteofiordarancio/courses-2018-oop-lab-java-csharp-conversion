package it.unibo.oop.events;

@FunctionalInterface
public interface EventListener<Arg> {
    void onEvent(Arg data);
}
